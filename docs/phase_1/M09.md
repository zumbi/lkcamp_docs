## Meeting M9  

**Mini-presentation:**  Memory and scheduling  

Take a look at the project suggestions in the next session, choose a topic (it doesn't need to be one of those suggestions). We will form groups so people can help each other and prepare mini-presentation on the work they are doing.  

**Recommended Reading**: Linux Device Drivers, All remaining chapters  

Don’t forget to update the spreadsheet for tracking our progress.  

