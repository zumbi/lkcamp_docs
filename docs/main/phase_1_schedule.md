# Phase 1: Schedule

**M1)** know where to make questions, boot a custom kernel  

**M2)** know how to perform simple modifications in a module and test it  

**M3)** submit your patch and your patchset to the community  

**M4)** working with modules  

**M5)** optimizing development environment and debug techniques  

**M6)** talks about specific kernel subsystems (kernel and userspace interactions, video4linux)  

**M7)** talks about specific kernel subsystems (TTY and networking)  

**M8)** talks about specific kernel subsystems (Graphics and file systems)  

**M9)** talks about specific kernel subsystems (Memory and scheduling)  

**M10)** presentation about project suggestion for phase 2 and **pizza** to celebrate the end of phase 1  

** >M10)** Phase 2: work on specific project, presentations from students  

